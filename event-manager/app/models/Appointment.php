<?php

	class Appointment extends Eloquent
	{
    public static $rules = array(
    );
    protected $fillable = array('title', 'description','date');
    protected $softDelete = true;
	}