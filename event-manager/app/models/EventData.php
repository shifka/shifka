<?php

class EventData implements JsonSerializable {
      
 public function __construct(array $addedEvents,array $updatedEvents,array $deletedEvents) {
        $this->addedEvents = $addedEvents;
        $this->updatedEvents=$updatedEvents;
        $this->deletedEvents=$deletedEvents;
    }
    
    public function jsonSerialize() {
        
        return array(
                        'addedEvents' => $this->addedEvents,
                         'updatedEvents' => $this->updatedEvents,
                          'deletedEvents' => $this->deletedEvents,
        );
    }
}