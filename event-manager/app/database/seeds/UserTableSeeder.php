// app/database/seeds/UserTableSeeder.php

<?php

class UserTableSeeder extends Seeder
{

	public function run()
	{
		DB::table('users')->delete();
		User::create(array(
			'username'     => 'nip',
			'firstname' => 'Nipuni',
			'lastname' => 'Attanayake',
			'email'    => 'nipunishashipraba@gmail.com',
			'password' => Hash::make('n'),
		));
	}

}
