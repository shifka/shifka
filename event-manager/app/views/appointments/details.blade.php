@extends('layouts.main')

@section('main')
<div class="row">
<div class="col-md-9">
	<h1>{{$group->title}}</h1> 
</div>
<div class="col-md-1">
	{{ HTML::linkAction('AppointmentController@edit', 'Edit', array($group->id),array('class'=>"btn btn-primary") )}}
  {{ HTML::linkAction('AppointmentController@index', 'Return to list', array(),array('class'=>"btn btn-primary") )}}
</div>
</div>
<div class="row">
<p>{{$group->description}}</p>


@stop