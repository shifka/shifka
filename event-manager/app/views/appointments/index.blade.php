@extends('layouts.main')

@section('main')

<h1>Events</h1>

<p>{{ link_to_route('appointments.create', 'Add new event') }}</p>

@if ($events->count())
<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<td>Title</td>
			<td>Description</td>
			<td>Date</td>
		</tr>
	</thead>
	<tbody>
	@foreach($events as $key => $value)
		<tr>
			<td>{{ $value->title }}</td>
			<td>{{ $value->description }}</td>
			<td>{{ $value->date }}</td>
			
		
			<td>

				{{ HTML::linkAction('AppointmentController@show', 'View', array($value->id),array('class'=>"btn btn-primary") )}} </td>
                    <td>   
			  {{ Form::open(array('method' 
=> 'DELETE', 'route' => array('appointments.destroy', $value->id))) }}                       
                            {{ Form::submit('Delete', array('class'
 => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        </td>
			     
			</td>
		</tr>
	@endforeach
	</tbody>
</table>
@else
    There are no groups
@endif
@stop