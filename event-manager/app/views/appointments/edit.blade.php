@extends('layouts.main')

@section('main')

<h1>Edit Group</h1>
    {{ Form::model($group, array('method' => 'PATCH', 'route' =>array('appointments.update', $group->id))) }}
        <div class="row">
		<ul>
            <li>
                {{ Form::label('title', ' Title:') }}
                {{ Form::text('title') }}
            </li>

            <li>
                {{ Form::label('description', 'Description:') }}
                {{ Form::text('description') }}
            </li>
            <li>
                {{ Form::label('date', 'Date:') }}
                {{ Form::text('date', null,array('class' => 'datepicker')) }}
            </li>
         </ul>
         </div>
		 <div class="row">
                {{ Form::submit('Update', array('class' => 'btn')) }}
         </div>      
            
       
    {{ Form::close() }}

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

@stop