@extends('layouts.main')

@section('main')

<h1>Create Group</h1>

{{ Form::open(array('route' => 'appointments.store')) }}
    <ul>

        <li>
            {{ Form::label('gname', ' Title:') }}
            {{ Form::text('title') }}
        </li>

        <li>
            {{ Form::label('description', 'Description:') }}
            {{ Form::text('description') }}
        </li>

        <li>
            {{ Form::label('date', 'Date:') }}
            {{ Form::text('date', '', array('class' => 'datepicker')) }}
        </li>
       
        <li>
            {{ Form::submit('Submit', array('class' => 'btn')) }}
        </li>
    </ul>
{{ Form::close() }}

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

@stop