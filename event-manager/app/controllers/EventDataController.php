<?php

class EventDataController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function getData($lastSync)
	{
		
    $format = 'Y-m-d H:i:s';
    $date = DateTime::createFromFormat($format, $lastSync);  
        $inserted=Appointment::where('created_at','>',$date,'AND')->get()->toArray();
        
       
    $updated=Appointment::where('updated_at','>',$date,'AND')->where('created_at','<',$date)->get()->toArray();

   $deleted=Appointment::onlyTrashed()->where('deleted_at','>',$date)->get(array('id'))->toArray();
   // $inserted=Appointment::where('created_at','>',$date);
  //  $updated=Appointment::where('updated_at','>',$date);
   // $deleted=Appointment::where('deleted_at','>',$date)->lists('id');
    
   return json_encode(new EventData($inserted,$updated,$deleted),JSON_PRETTY_PRINT);
	}
}
