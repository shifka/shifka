<?php

class AppointmentController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
	// get all the nerds
		$events = Appointment::all();
    
		// load the view and pass the nerds
	 return View::make('appointments.index', compact('events'));
	
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
	 return View::make('appointments.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
	 $input = Input::all();
        $validation = Validator::make($input, Appointment::$rules);

        if ($validation->passes())
        {   
                      Appointment::create($input);
            	$events = Appointment::all();
            return Redirect::route('appointments.index', compact('events'));
        }

        return Redirect::route('appointments.create')
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	$group = Appointment::find($id);       // Eloquent object's find method will query the database just like a normal SQL.
                                        //Select * from groups where id = $id
    if (is_null($group))
    {
      return Redirect::route('appointments.index');
    }
    return View::make('appointments.details', compact('group'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
	$group = Appointment::find($id);       // Eloquent object's find method will query the database just like a normal SQL.
                                        //Select * from groups where id = $id
    if (is_null($group))
    {
      return Redirect::route('appointments.index');
    }
    return View::make('appointments.edit', compact('group'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
	 $input = Input::all();
        $validation = Validator::make($input, Appointment::$rules);
        if ($validation->passes())
        {
            $user = Appointment::find($id);
            $user->update($input);
            return Redirect::route('appointments.show', $id);
        }
return Redirect::route('appointments.edit', $id)
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		  Appointment::find($id)->delete();
        return Redirect::route('appointments.index');
	}
  


}
